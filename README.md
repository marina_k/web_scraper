# Book Scraper

A simple scraper to extract product description and info from [books.toscrape](http://books.toscrape.com/index.html) using Python Requests.

## Installation

Install beautifulsoup and requests

    pip3 install beautifulsoup4 requests

## How to Use

If you want to fetch info for books in categories other than poetry and science, add book categories to the categories list:

    categories = ['poetry','science']

 To run 

    python3 books_to_scrape.py

## Sample Output

    books_to_scrape.csv
