import pandas as pd
import requests,re,pprint
from bs4 import BeautifulSoup
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine

engine = create_engine('sqlite://', echo=False)
categories = ['poetry','science']
category_urls =[]
books_url = []
titles = []
meta_data =[]
base_url = 'http://books.toscrape.com/'

def get_soup(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.content, "html.parser")
    return soup

def find_url(subject):
    base_soup = get_soup(base_url)
    href = base_soup.find(href=re.compile(subject +"_")).get('href')
    final_url = base_url + href
    return final_url

def html_table(url):
    table = pd.read_html(url)[0].set_index(0).T 
    return table 

#get category urls and append to list
for category in categories:
    category_url = find_url(category)
    category_urls.append(category_url) 
         
#get urls of all books from poetry and science sections. Append to list
for url in category_urls:
    book = get_soup(url).findAll('div', attrs={'class':'image_container'})
    for i in book:
        href = i.find('a').get('href')
        href = href.replace('../../../','')
        book_url = base_url +'catalogue/'+ href
        books_url.append(book_url)

#get product title and description
for url in books_url:
    meta = get_soup(url).find("meta",attrs={"name":"description"}).get('content').strip()
    meta_data.append(meta)
    title = get_soup(url).find('h1').text.strip()
    titles.append(title)

df = pd.concat([html_table(url) for url in books_url])
df['Title'] = titles
df['Description'] = meta_data

#export to csv
df.to_csv("books_to_scrape.csv", index=False)

#export to sql
df.to_sql('books', con=engine)
#pprint.pprint(engine.execute("SELECT * FROM books").fetchall())



